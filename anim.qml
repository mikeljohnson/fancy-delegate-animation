/*
    Copyright (C) 2021 by Mikel Johnson <mikel5764@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

import QtQuick 2.12
import org.kde.plasma.core 2.0 as PlasmaCore
import QtQuick.Controls 2.15
ApplicationWindow {
    id: window
    visible:true
    width:500
    height:500
    header: ToolBar {
        Row {
            anchors.centerIn: parent
            ToolButton { text: "set up"; onClicked: window.setup() }
            ToolButton { text: "fade in"; onClicked: window.fadein() }
            ToolButton { text: "fade out"; onClicked: window.fadeout() }
        }
    }
    property var objectMap: { "":"" }
    function setup() {
        var ind = listView.indexAt(1, listView.contentY)
        for (var i = 0; i <listView.count; i++){
            if(listView.itemAtIndex(i)){
                var newObject = Qt.createQmlObject(
                'import org.kde.plasma.core 2.0 as PlasmaCore; import QtQuick 2.5;' + 
                'Translate { Behavior on y { NumberAnimation { duration: PlasmaCore.Units.shortDuration } } }',
                this,
                'newObject');
                listView.itemAtIndex(i).transform.push(newObject)
                objectMap[listView.itemAtIndex(i)] = newObject
            }
        }
    }
    function fadein() {
        var ind = listView.indexAt(1, listView.contentY)
        for (var i = 0; i <listView.count; i++){
            if(listView.itemAtIndex(i)){
            if(objectMap[listView.itemAtIndex(i)] != undefined) {
                objectMap[listView.itemAtIndex(i)].y = 0
            }
            listView.opacity = 1
            }
        }
    }
    function fadeout() {
        var ind = listView.indexAt(1, listView.contentY)
        for (var i = 0; i <listView.count; i++){
            if(listView.itemAtIndex(i)){
            if(objectMap[listView.itemAtIndex(i)] != undefined) {
                objectMap[listView.itemAtIndex(i)].y = i**1.5* listView.itemAtIndex(i).height + listView.itemAtIndex(i).height
            }
            listView.opacity = 0
            }
        }
    }
    Rectangle {
        anchors.fill: parent
        color: "#fafafa"
        ListView {
            id: listView
            anchors.fill: parent
            model: 20
            spacing: PlasmaCore.Units.smallSpacing
            topMargin: spacing
            leftMargin: spacing
            rightMargin: spacing

            Behavior on opacity { NumberAnimation { duration: PlasmaCore.Units.shortDuration } }

            delegate: Rectangle {
                width: listView.width - listView.spacing * 2
                height: PlasmaCore.Units.smallSpacing * 4 + PlasmaCore.Units.iconSizes.smallMedium
                border.width: 1
                border.color: "#22000000"
                radius: 4
                Text {
                    anchors.centerIn: parent
                    text: "Item " + index
                }
            }

            focus: true
            Keys.onSpacePressed: model.insert(0)
        }
    }
}
 
