/*
    Copyright (C) 2021 by Mikel Johnson <mikel5764@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.plasma.core 2.0 as PlasmaCore
import QtQuick.Controls 2.15
Item {
    width:500
    height:500
    Button {
        text: "animate"
        z: 50
    onClicked: {
        var ind = listView.indexAt(1, listView.contentY)
        for (var i = 0; i <listView.count; i++){
            if(listView.itemAtIndex(i)){
            listView.itemAtIndex(i).trans = 0//i**1.5* 20//listView.itemAtIndex(i).height/(listView.count-(ind-i))*20
            }
        }
    }
    Button {
        text: "reset"
        anchors.left: parent.right
        z: 50
    onClicked: {
        var ind = listView.indexAt(1, listView.contentY)
        for (var i = 0; i <listView.count; i++){
            if(listView.itemAtIndex(i)){
            listView.itemAtIndex(i).trans = i**1.5* 20+20//listView.itemAtIndex(i).height/(listView.count-(ind-i))*20
            }
        }
    }
    }
    }
    ListView {
        id: listView
        anchors.fill: parent
        model: 20
        spacing: 4
        topMargin: 4
        leftMargin: 4
        rightMargin: 4
        delegate: Rectangle {
            width: parent.width; height: 30
            border.width: 1
            color: "#fefefe"
            Text {
                anchors.centerIn: parent
                text: "Item " + index
            }
            property int trans: index**1.5* 20+20
            opacity: trans == 0
            Behavior on opacity { NumberAnimation { duration: PlasmaCore.Units.shortDuration } }
            transform: Translate {
                y: trans
//                 onYChanged: y = 0
                Behavior on y { NumberAnimation { duration: PlasmaCore.Units.shortDuration } }
            }
        }

        focus: true
        Keys.onSpacePressed: model.insert(0)
    }
} 
 
